package com.conroller;

import com.domain.Video;
import com.peros.VideoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
    @Autowired
    private VideoRepo videoRepo;

    @GetMapping("/")
    public String greeting (Map<String,Object> model) {

        return "greeting";
    }

    @GetMapping("/main")
    public String main(Map<String,Object> model){

        Iterable<Video> videos = videoRepo.findAll();
        model.put("videos",videos);
        return "main";
    }
    @PostMapping("/main")
    public String add(@RequestParam String link,@RequestParam String t1,@RequestParam String t2, Map<String,Object> model){

        Video video = new Video(link,t1,t2);

        videoRepo.save(video);

        Iterable<Video> videos = videoRepo.findAll();
        model.put("videos",videos);

        return "main";
    }
   // @PostMapping("/logout")
    //public String logout(Map<String,Object> model){
      //  return "redirect:/login";
    //}
}