package com.peros;

import com.domain.Video;
import org.springframework.data.repository.CrudRepository;

public interface VideoRepo extends CrudRepository <Video, Long> {
}
